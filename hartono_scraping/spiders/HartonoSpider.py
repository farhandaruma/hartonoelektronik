import scrapy
import csv
from urllib import parse

class HartonoSpider(scrapy.Spider):

    name = 'hartono_spider'

    def start_requests(self):
        result = []
        with open('hartono.csv', 'r', errors='ignore') as csvfile:
            csvreader = csv.DictReader(csvfile)
            for row in csvreader:
                result.append(
                    dict(
                        name=row['Nama untuk Upload'],
                        mpn=row['MPN']
                    )
                )

        for url in result:
            search = f'https://www.hartonoelektronika.com/en/?subcats=Y&status=A&pshort=Y&pfull=Y&pname=Y&pkeywords=Y&search_performed=Y&advanced_filter=Y&match=all&q={parse.quote_plus(url["mpn"])}&dispatch=products.search'
            scrapy.Request(url=search, callback=self.parse_get_product, meta={'mpn':url['mpn']})

    def parse_get_product(self, response):
        product_url = response.css('.product-title-wrap .product-title::attr(href)').extract_first()
        if product_url:
            scrapy.Request(url=f'https://www.hartonoelektronika.com{product_url}', callback=self.parse_product_page)

    def parse_product_page(self, response):
        image_url = f"https://www.hartonoelektronika.com{response.css('.cm-image-previewer::attr(href)').extract_first()}"
        mpn = response.meta['mpn']